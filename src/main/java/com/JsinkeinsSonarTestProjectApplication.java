package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsinkeinsSonarTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsinkeinsSonarTestProjectApplication.class, args);
	}

}
